//
//  NetworkCommunicationProvider.swift
//  Neti
//

import UIKit
import Alamofire
import AlamofireImage
import AlamofireObjectMapper
import ObjectMapper

class NetworkCommunicationProvider: NSObject {
    func downloadPhotos(_ result : @escaping (Array<PhotoModelObject>? , Error?) -> () ) {
        
        Alamofire.request("https://jsonplaceholder.typicode.com/photos").responseArray { (response: DataResponse<[PhotoModelObject]>) in
            switch response.result {
            case .success(let items):
               
                let photos = items
                result(photos, nil)

            case .failure(let error):
                result(nil, error)
            }
        }
    }
    
    func dowloadImageForPhoto(_ model:PhotoModelObject, result: @escaping (UIImage?, Error?) -> ()) {
        Alamofire.request(model.url).responseImage{ response in
            if let image = response.result.value {
                result(image,nil)
            }
        }
    }
}
