//
//  PhotoModelObject.swift
//  Neti
//
//  Created by Michal Niemiec on 09/11/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import UIKit
import RealmSwift
import AlamofireObjectMapper
import ObjectMapper

class PhotoModelObject: Object, Mappable {
    @objc dynamic var albumId = 0
    @objc dynamic var id = 0
    @objc dynamic var title = ""
    @objc dynamic var url = ""
    @objc dynamic var thumbnailUrl = ""
    var image = UIImage()
    
    func buildModel(albumId: Int, id: Int, title: String, url: String, thumbnailUrl: String) {
        self.albumId = albumId
        self.id = id
        self.title = title
        self.url = url
        self.thumbnailUrl = thumbnailUrl
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    func mapping(map: Map) {
        id    <- map["id"]
        albumId <- map["albumId"]
        title <- map["title"]
        url <- map["url"]
        thumbnailUrl <- map["thumbnailUrl"]
    }
}
