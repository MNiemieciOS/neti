//
//  DatabaseHandler.swift
//  Neti
//


import UIKit
import RealmSwift

struct PhotoForImage {
    var image : UIImage?
    var photoModel : PhotoModelObject?
}

class DatabaseHandler: NSObject {
    var realmObject: Realm?
    var privatePhoto : [PhotoModelObject]?
    var pageNumber = 0
    let pageSize = 15
    var imageForPhotosArray = [PhotoForImage]()
    var imageStorage = ImageStorage()
    
    fileprivate func saveModelAndPhoto(_ image: UIImage?,model: PhotoModelObject) {
        let newPhoto = PhotoForImage(image: image, photoModel: model)
        self.imageForPhotosArray.append(newPhoto)
        ImageStorage.save(image: image!, product: model)
    }
    
    func downloadImage(_ network: NetworkCommunicationProvider,result: @escaping (Bool) -> ()) {
        if pageNumber * pageSize < (privatePhoto?.count)! {
            let slice = privatePhoto?[pageNumber*pageSize...pageSize*(pageNumber+1)]
            
            let imageGroup = DispatchGroup()
            for model in slice! {
                imageGroup.enter()
                if let image = loadPhotoForModel(model) {
                    saveModelAndPhoto(image,model: model)
                    imageGroup.leave()
                } else {
                    network.dowloadImageForPhoto(model, result: { (image, error) in
                        if error == nil {
                            self.saveModelAndPhoto(image,model: model)
                            imageGroup.leave()
                        } else {
                            print(error.debugDescription)
                            imageGroup.leave()
                        }
                    })
                }
            }
            imageGroup.notify(queue: .main) {
                self.savePhotos()
                self.pageNumber += 1
                result(true)
            }
        } else {
            result(true)
        }
    }
    
    func halfOfCurrentPage() -> Int {
        return Int(Double(pageSize) * 0.5) + pageSize  * (pageNumber - 1)
    }
    
    func savePhotos() {
        let photos = imageForPhotosArray.map { (photo) -> PhotoModelObject in
            return photo.photoModel!
        }
        do {
            realmObject = try Realm()
            let photosToSave = photos.filter({ (photoModel) -> Bool in
                if ((realmObject?.object(ofType: PhotoModelObject.self, forPrimaryKey: photoModel.id)) == nil) {
                    return true
                }
                return false
            })
          
            try realmObject?.write({
                realmObject?.add(photosToSave)
            })
        } catch {
        }
    }
    
    func loadPhotoForModel(_ model: PhotoModelObject) -> UIImage? {
        do {
            realmObject = try Realm()
            if ((realmObject?.object(ofType: PhotoModelObject.self, forPrimaryKey: model.id)) != nil) {
                return ImageStorage.loadImage(for: model)
            } else {
                return nil
            }
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
}
