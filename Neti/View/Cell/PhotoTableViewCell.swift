//
//  PhotoTableViewCell.swift
//  Neti
//
//  Created by Michal Niemiec on 08/11/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import UIKit

class PhotoTableViewCell: UITableViewCell {
    @IBOutlet private weak var photoImageView: UIImageView!
    @IBOutlet private weak var photoLabel: UILabel!
    func configure(withPhotoModel model: PhotoModelObject, photoImage:UIImage) {
        photoLabel.text = model.title
        photoImageView.image = photoImage
    }
}
