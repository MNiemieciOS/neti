//
//  MainViewController.swift
//  Neti
//
//  Created by Michal Niemiec on 09/11/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import UIKit
import SCLAlertView

class MainViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var photosViewModel = PhotosViewModel()
    var isDownloadingData = false
    
    let offlineCommunicate = "You're in Offline Mode! Check your internet connection."
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
        
        photosViewModel.getSamplePhotos { [weak self] (downloadSuccess) in
            if downloadSuccess {
                self?.tableView.reloadData()
            } else {
                print("Error")
                SCLAlertView().showError("Error", subTitle: (self?.offlineCommunicate)!)
            }
        }
    }
}

// MARK: - Table view data source
extension MainViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return photosViewModel.returnNumberOfElements()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "photoCell", for: indexPath)
        return cell
    }
}

extension MainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row > photosViewModel.getHalfOfSize() {
            if isDownloadingData == false {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
                isDownloadingData = true
                photosViewModel.getNewData({ (done) in
                    self.tableView.reloadData()
                    self.isDownloadingData = false
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                })
            }
        } else {
            if let photoCell = cell as? PhotoTableViewCell {
                if let photoModel = photosViewModel.getPhotoForIndex(indexPath.row) {
                    photoCell.configure(withPhotoModel: photoModel.model,photoImage :photoModel.image)
                }
            }
        }
    }
}
