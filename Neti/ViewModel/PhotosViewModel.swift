//
//  PhotosViewModel.swift
//  Neti
//
//  Created by Michal Niemiec on 08/11/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import UIKit

class PhotosViewModel {
    private let database = DatabaseHandler()
    private let network = NetworkCommunicationProvider()
    
    func returnNumberOfElements () -> Int {
        return database.imageForPhotosArray.count
    }
    
    func getHalfOfSize() -> Int {
        return database.halfOfCurrentPage()
    }
        
    func getNewData(_ completion: @escaping(Bool) -> ()) {
        getImageForPhotos { (done) in
            completion(done)
        }
    }
    
    func getPhotoForIndex(_ index: Int) -> (model: PhotoModelObject, image:UIImage)? {
        if let photoModel = database.privatePhoto?[index], let imageModel = database.imageForPhotosArray.first(where: { (photo) -> Bool in
            photo.photoModel?.id == index+1
        })?.image {
            return (photoModel,imageModel)
        } else {
            return nil
        }
    }
    
    fileprivate func getImageForPhotos(_ completion:@escaping (Bool) -> ()) {
        self.database.downloadImage(self.network, result: { (done) in
            self.database.savePhotos()
            completion(true)
        })
    }
    
    func getSamplePhotos (_ completion : @escaping (Bool) -> ()) {
        network.downloadPhotos { [weak self] (photosArray, error) in
            if error == nil {
                self?.database.privatePhoto = photosArray
                self?.getImageForPhotos(completion)
            } else {
                completion(false)
            }
        }
    }
}
