//
//  ImageStorage.swift
//  Neti
//
//  Created by Michal Niemiec on 18/11/2017.
//  Copyright © 2017 MNiemiec. All rights reserved.
//

import UIKit

class ImageStorage: NSObject {
    static private var path: URL?
    
    class func save(image: UIImage, product: PhotoModelObject) {
        self.save(image: image, name: String(product.id))
    }
    
    private class func save(image: UIImage, name: String) {
        if let path = self.generatePathFor(name: name), let data = UIImagePNGRepresentation(image) {
            do {
                if !checkIfImageExist(file: path) {
                    FileManager.default.createFile(atPath: path.path, contents: data, attributes: nil)
                    try  data.write(to: path)
                }
            } catch let error as NSError {
                print("Error : \(error.localizedDescription)")
            }
            
        }
    }
    
    private class func generatePathFor(product:PhotoModelObject) -> URL? {
        return self.generatePathFor(name: String(product.id))
    }
    
    private class func generatePathFor(name:String) -> URL? {
        if let basePath = self.getBasePath() {
            let filePath = basePath.appendingPathComponent(name).appendingPathExtension("png")
            return filePath
            
        }
        return nil
    }
    
    class func loadImage(for product: PhotoModelObject) -> UIImage? {
        if let imagePath = self.generatePathFor(product: product),
            let img = NSData(contentsOfFile: imagePath.path) {
            let image = UIImage(data: img as Data)
            return image
        }
        return nil
    }
    
    private class func getBasePath() -> URL? {
        if let basePath = self.path {
            return basePath
        } else {
            if let documentsURL = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)  {
                let directory = documentsURL.appendingPathComponent("PhotoImages")
                if FileManager.default.fileExists(atPath: directory.path, isDirectory: nil) {
                    print("directory exist")
                    self.path = directory
                    return directory
                } else {
                    print("file path dont exist")
                    do {
                        try FileManager.default.createDirectory(atPath: directory.path, withIntermediateDirectories: true, attributes: nil)
                        self.path = directory
                        return directory
                    } catch let error as NSError {
                        print("Error creating directory: \(error.localizedDescription)")
                        return nil
                    }
                }
            }
            return nil
        }
    }
    
    private class func checkIfImageExist(file: URL) -> Bool {
        var isDir : ObjCBool = false
        if FileManager.default.fileExists(atPath: file.path, isDirectory: &isDir) {
            return true
        }
        return false
    }
}
